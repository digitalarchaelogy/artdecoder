﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

using UINTW = System.UInt32;
using JGPTR = System.IntPtr;
using JGERR = System.UInt32; //4th nibble is category, bottom 3 nibbles are specifics
using JGBOOL = System.Int32;
using JGFOURCHAR = System.UInt32;
using JGDW_HDEC = System.IntPtr;
using JGDW_HBITMAP = System.IntPtr;
using System.Diagnostics;
using System.IO; /* JGDW_RECORD */

namespace ArtDecoder
{
    public static class JgInterop
    {
        #region Calls

        [DllImport("JGDW400.dll")]
        public static extern UInt32 JgSetDecompressResourceBuffer(IntPtr buffer, UInt32 length);

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgCreateDecompressContext(
            [In,Out] ref JGDW_CONTEXT hContext,        // OUT: context handle
            [In] ref JG_DECOMPRESS_INIT InitStruct  // IN: filled init structure
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgCreateDecompressor(
            [In,Out] ref JGDW_HDEC hDec,                // OUT: decompression handle
            [In] ref JGDW_CONTEXT hContext,              // IN: context handle
            [In] ref JG_DECOMPRESS_INIT Init        // IN: null, or override of context
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgDestroyDecompressor(
            JGDW_HDEC hDec                       // IN: decompression handle
        );



        [DllImport("JGDW400.dll")]
        public static extern JGERR JgDestroyDecompressContext(
            [In] ref JGDW_CONTEXT hContext               // IN: context handle
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgInitDecompress(
            [In,Out] ref JGDW_HDEC hJgImageOutput,        /* A pntr to recve the Img handle. */
            [In] ref JG_DECOMPRESS_INIT InitStruct  /* A filled init structure */
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgDecompressImageBlock(
            JGDW_HDEC hJgImage,           /* Handle to Decompress Struct */
            [In] byte[] pImageBuf, /* Input buffer of compressed image data */
            UInt32 nBufSize,             /* Number of bytes of data in buffer */
            [In,Out] ref JGBOOL bNewData          /* True if new data are available */
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgGetDecompressInfo(
            JGDW_HDEC hJgImage,             /* Handle to Decompress Struct */
            [In,Out] ref JG_DECOMPRESS_INFO Info  /* Out: Filled Info struct */
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgGetImage(
            JGDW_HDEC hJgImage,             /* Handle to Decompression Structure */
            [In, Out] ref JGDW_HBITMAP hBitmap      /* Output Handle to bitmap, if it exists */
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgDecompressDone(
            JGDW_HDEC hJgImage              /* Handle to Decompress Struct */
        );


        [DllImport("JGDW400.dll")]
        public static extern JGERR JgQueryArtImage(
            /*UINT8 JGHPTR*/ [In] byte[] pBuf,      /* First bytes of the compressed image */
            UInt32 nBufSize              /* Size of Buffer */
        );

        [DllImport("JGDW400.dll")]
        public static extern JGERR JgGetImageInfo(
            [In] byte[] pBuf,      /* First bytes of the compressed image */
            UInt32 nBufSize,             /* Number of bytes in buffer */
            [In, Out] ref JG_RAWIMAGE_INFO Info
        );

        #endregion Calls

        #region Constants

        public const UInt32 JG_OPTION_FULLIMAGE = 0x0008;
        public const UInt32 JG_OPTION_INHIBIT_AUDIO = 0x20;

        public const UInt32 JG_POSTSCALE_LONGSIDE = 0x0001;
        public const UInt32 JG_GAMMA_NONE = 100;

        #endregion

        #region Structs

        [StructLayout(LayoutKind.Sequential)]
        public struct JG_DECOMPRESS_INIT {
            UINTW nSize;            /* Size of this structure, set be caller */
            UINTW ColorDepth;       /* Color depth to use (4, 8, or 24) */
            UINTW DecodeOptions;    /* Decoding Options */
            JGPTR DefaultPalette; /* Default Palette, or NULL if none */
            UINTW PaletteSize;      /* Size of Default Palette, if any */
            UINTW SplashDupFactor;  /* Replication Factor of Miniature in Full */
                                   /*    image; 0=Off, >100=Full Size */
            JGBOOL bTrueSplash;      /* Save splash image until completely rdy */
            UINTW PostScaleFlags;   /* Defines Post Scale.  Or of JG_POSTSCALE_xxx */ 
            UINTW ScaledLongSide;   /* Used for Post scale, for JG_POSTSCALE_LONGSIDE */
            UINTW ScaledX;          /* Used for Post scale, for JG_POSTSCALE_X */
            UINTW ScaledY;          /* Used for Post scale, for JG_POSTSCALE_Y */
            JGDW_COLOR BackgroundColor; /* Used to specify background color */
            UINTW AudioOptions;     /* Defined elsewhere (in JGAW.H) */
	        JGFOURCHAR ImageFormat;/* Specify image format or 'auto' for autodetect */
	        UINTW GammaIn;          /* Default input gamma correction */
	        UINTW GammaOut;         /* Desired output gamma correction */
            UINTW TransIndex;       /* Make this color transparent */

            public JG_DECOMPRESS_INIT(UInt32 longside)
            {
                nSize = (UINTW)(Marshal.SizeOf(typeof(JG_DECOMPRESS_INIT)) - Marshal.SizeOf(typeof(UINTW)));
                ColorDepth = 24;
                DecodeOptions = JG_OPTION_FULLIMAGE | JG_OPTION_INHIBIT_AUDIO;
                DefaultPalette = IntPtr.Zero;
                PaletteSize = 0;
                SplashDupFactor = 0;
                bTrueSplash = 1;
                //PostScaleFlags = JG_POSTSCALE_LONGSIDE;
                PostScaleFlags = 0;
                ScaledLongSide = 0; //???
                ScaledX = longside;
                ScaledY = longside;
                BackgroundColor = default(JGDW_COLOR);
                AudioOptions = 0; //??
                //0x41525466; //'ARTf'
                ImageFormat = 0x6175746F; //'auto'
                GammaIn = JG_GAMMA_NONE;
                GammaOut = JG_GAMMA_NONE;
                TransIndex = 0;
            }
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct JG_RAWIMAGE_INFO {
            public UINTW  nSize;                /* Size of this structure, set by caller */
            public UINTW  Version;              /* File's Version */
            public UINTW  SubVersion;           /* File's SubVersion */
            public JGBOOL  Decodeable;          /* Nonzero if file can be decoded */
                                         /* The following elements are only */
                                         /* Valid if the image is decodeable */
            public UINTW  Rows;                 /* Actual Rows at compress time */
            public UINTW  Cols;                 /* Actual Cols at compress time */
            public JGBOOL  HasPalette;          /* If Nonzero, image contains a palette */
            public JGBOOL  HasOverlays;         /* If Nonzero, image has enhancements */
            public JGFOURCHAR ImageFormat;      /* Four-character image type code */
            public UINTW  ColorDepth;           /* Native color depth of image */

            public static JG_RAWIMAGE_INFO Create()
            {
                return new JG_RAWIMAGE_INFO()
                {
                    nSize = (UINTW)(Marshal.SizeOf(typeof(JG_RAWIMAGE_INFO)) - Marshal.SizeOf(typeof(UINTW)))
                };
            }
        } ;

        public struct JG_DECOMPRESS_INFO
        {
            public UINTW  nSize;                /* Size of this structure, set by caller */
            public JGBOOL  bError;              /* Out: True if error detected which
                                            prevents further decoding */
            public JGBOOL  bImageDone;          /* True if no further input required */
            public JGBOOL  bNewNormalPixels;    /* True if Pixels ready in normal image */
            public JGBOOL  Reserved1;
            public JGDW_RECT  OldRect;          /* For compatibility */
            public JGDW_RECT  UpdateRect;       /* Update region of image */
            public UINTW  PaletteMode;          /* Type of palettizing being done */
            public JGERR  iErrorReason;         /* Status code for error, if any */
            public JGFOURCHAR ImageFormat;      /* Format of compressed image */
            public UINTW	PaletteColors;	     /* Number of colors in palette */
            public UINTW TransIndex;		     /* Index of transparent color (0xffff=none) */

            public static JG_DECOMPRESS_INFO Create()
            {
                return new JG_DECOMPRESS_INFO()
                {
                    nSize = (UINTW)(Marshal.SizeOf(typeof(JG_DECOMPRESS_INFO)) - Marshal.SizeOf(typeof(UINTW)))
                };
            }
        };

        [StructLayout(LayoutKind.Sequential)]
        public struct JGDW_CONTEXT
        {
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct JGDW_RECORD
        {
        }

        //Alias JGDW_PALETTEENTRY
        [StructLayout(LayoutKind.Sequential)]
        public struct JGDW_COLOR
        {
            byte peRed;
            byte peGreen;
            byte peBlue;
            byte peFlags;
        }

        //https://docs.microsoft.com/en-us/windows/win32/api/windef/ns-windef-rect
        [StructLayout(LayoutKind.Sequential)]
        public struct JGDW_RECT //Alias RECT
        {
            Int32 left;
            Int32 top;
            Int32 right;
            Int32 bottom;
        };

        /*
         * Guessed at bitmap header structure.
         * A known 210x210 image decoded at 24 bit produced the following header
         * 28 00 00 00 D2 00 00 00 D2 00 00 00 01 00 18 00
         * 00 00 00 00 70 06 02 00 00 00 00 00 00 00 00 00
         * 00 00 00 00 00 00 00 00 (Pixel data followed)
         * 
         * Notable things:
         * 0x28 = 40 = length of header
         * 0xD2 = 210 = known width/height. 
         * 0x18 = 24 = color depth
         * 0x70 06 02 00 = UInt32 LE 132720, which is about the projected size of the pixel data
         * 132720 / 210 = 632
         * This is probably the stride, as 632/3 is almost 210, and 632 is divisible by 4 which
         * is a common convention
         * */
        [StructLayout(LayoutKind.Sequential, Pack=1)]
        public struct GuessedBitmapHeader
        {
            public UInt32 nSize;
            public UInt32 nWidth;
            public UInt32 nHeight;
            public UInt16 nUnknown;
            public UInt16 nColorDepth;
            public UInt32 nUnknown2;
            public UInt32 nBytesOfPixelData;
            public UInt32 nUnknown3;
            public UInt32 nUnknown4;
            public UInt32 nUnknown5;
            public UInt32 nUnknown6;
        };

        #endregion Structs

        public static bool QueryImage(byte[] compressedBytes)
        {
            JGERR getInfoResult = JgQueryArtImage(compressedBytes, (UInt32)compressedBytes.Length);
            return getInfoResult == 0;
        }

        public static JG_RAWIMAGE_INFO GetImageInfo(byte[] compressedBytes)
        {
            JG_RAWIMAGE_INFO rawImageInfo = JG_RAWIMAGE_INFO.Create();
            JGERR getInfoResult = JgGetImageInfo(compressedBytes, (UInt32)compressedBytes.Length, ref rawImageInfo);
            Debug.Assert(getInfoResult == 0);
            return rawImageInfo;
        }


        public const UInt32 ResourceBufferLength = 500 * 1024 * 1024;
        public static IntPtr ResourceBuffer = IntPtr.Zero;

        public static void Initialize()
        {
            ResourceBuffer = Marshal.AllocHGlobal((int)ResourceBufferLength);
            JGERR setResourceBufferResult = JgSetDecompressResourceBuffer(ResourceBuffer, ResourceBufferLength);
            Debug.Assert(setResourceBufferResult == 0);
        }

        public static void Decode(byte[] compressedBytes, string outputFilePath)
        {
            var NULL_PTR = IntPtr.Zero;

            JG_RAWIMAGE_INFO info = GetImageInfo(compressedBytes);

            //Decompress Init
            JG_DECOMPRESS_INIT decompressInit = new JG_DECOMPRESS_INIT(256 /* ??? */);
            JGDW_CONTEXT decompressorContext = new JGDW_CONTEXT();
            JGERR decompressorContextResult = JgCreateDecompressContext(ref decompressorContext, ref decompressInit);
            Debug.Assert(decompressorContextResult == 0);

            JGDW_HDEC decoder = IntPtr.Zero;
            JGERR decoderResult = JgCreateDecompressor(ref decoder, ref decompressorContext, ref decompressInit);
            Debug.Assert(decoderResult == 0);


            //Decompress
            IntPtr hJgImage = IntPtr.Zero;
            JGERR initDecompressResult = JgInitDecompress(ref hJgImage, ref decompressInit);
            Debug.Assert(initDecompressResult == 0);

            //The original is set up to decode block by block, but we have all the data
            //so do it in one shot.
            JGBOOL wasNewData = 0;
            JGERR decompressBlockResult = JgDecompressImageBlock(hJgImage, compressedBytes, (UInt32)compressedBytes.Length, ref wasNewData);
            Debug.Assert(decompressBlockResult == 0);
            Debug.Assert(wasNewData > 0);
            //...and just to get extra info...
            JG_DECOMPRESS_INFO decompressInfo = JG_DECOMPRESS_INFO.Create();
            JGERR decompressInfoResult = JgGetDecompressInfo(hJgImage, ref decompressInfo);
            Debug.Assert(decompressInfoResult == 0);
            Debug.Assert(decompressInfo.bError == 0);

            //Now convert back to a normal image
            IntPtr hBitmapPtr = IntPtr.Zero;
            JGERR getImageResult = JgGetImage(hJgImage, ref hBitmapPtr);
            Debug.Assert(getImageResult == 0);

            //We got a pointer-pointer, so we need to dereference it
            var getPtrBytes = new byte[4];
            Marshal.Copy(hBitmapPtr, getPtrBytes, 0, getPtrBytes.Length);
            var hBitmapHeaderPtrValue = BitConverter.ToInt32(getPtrBytes, 0);
            IntPtr hBitmapHeaderPtr = new IntPtr(hBitmapHeaderPtrValue);

            //The output consists of a bitmap header of some sort followed by bitmap data
            //Get the header
            GuessedBitmapHeader? bitmapHeader = (GuessedBitmapHeader?)Marshal.PtrToStructure(hBitmapHeaderPtr, typeof(GuessedBitmapHeader));
            Debug.Assert(bitmapHeader.HasValue);
            Debug.Assert(bitmapHeader.Value.nSize == Marshal.SizeOf(typeof(GuessedBitmapHeader)));

            //Get the bitmap data
            IntPtr hBitmap = new IntPtr(hBitmapHeaderPtrValue + bitmapHeader.Value.nSize);

            var stride = info.Cols*3;
            while(stride % 4 != 0)
                stride++;
            var bitmapBytes = new byte[info.Rows*stride];
            Marshal.Copy(hBitmap, bitmapBytes, 0, bitmapBytes.Length);

            using (var bitmap = new System.Drawing.Bitmap((int)info.Cols, (int)info.Rows, System.Drawing.Imaging.PixelFormat.Format24bppRgb))
            {
                //We'll do the slow way until we know what is up
                for (int x = 0; x < info.Cols; x++)
                {
                    for (int y = 0; y < info.Rows; y++)
                    {
                        var pixelStartIndex = (stride * y + x*3);
                        var pixel = System.Drawing.Color.FromArgb(
                            bitmapBytes[pixelStartIndex + 2],
                            bitmapBytes[pixelStartIndex + 1],
                            bitmapBytes[pixelStartIndex + 0]
                            );
                        var tx = x;
                        var ty = (int)(info.Rows - 1) - y;
                        bitmap.SetPixel(tx, ty, pixel);
                    }
                }
                bitmap.Save(outputFilePath);
            }

            //Presumably we clean it up this way?
            Marshal.FreeHGlobal(hBitmapHeaderPtr);

            JGERR decompressDoneResult = JgDecompressDone(hJgImage);
            Debug.Assert(decompressDoneResult == 0);

            //Cleanup decompress init
            JGERR decoderCleanupResult = JgDestroyDecompressor(decoder);
            Debug.Assert(decoderCleanupResult == 0);
            JGERR decompressorContextCleanupResult = JgDestroyDecompressContext(ref decompressorContext);
            Debug.Assert(decompressorContextCleanupResult == 0);

            //Cleanup ResourceBuffer????
        }
    }
}
