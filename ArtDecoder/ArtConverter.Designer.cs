﻿namespace ArtDecoder
{
    partial class ArtConverter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnConvertArt = new System.Windows.Forms.Button();
            this.pbConvertedImage = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbConvertedImage)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnConvertArt
            // 
            this.btnConvertArt.Location = new System.Drawing.Point(12, 12);
            this.btnConvertArt.Name = "btnConvertArt";
            this.btnConvertArt.Size = new System.Drawing.Size(127, 23);
            this.btnConvertArt.TabIndex = 0;
            this.btnConvertArt.Text = "Convert File";
            this.btnConvertArt.UseVisualStyleBackColor = true;
            this.btnConvertArt.Click += new System.EventHandler(this.btnConvertArt_Click);
            // 
            // pbConvertedImage
            // 
            this.pbConvertedImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbConvertedImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbConvertedImage.Location = new System.Drawing.Point(12, 60);
            this.pbConvertedImage.Name = "pbConvertedImage";
            this.pbConvertedImage.Size = new System.Drawing.Size(752, 483);
            this.pbConvertedImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbConvertedImage.TabIndex = 1;
            this.pbConvertedImage.TabStop = false;
            // 
            // ArtConverter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 555);
            this.Controls.Add(this.pbConvertedImage);
            this.Controls.Add(this.btnConvertArt);
            this.Name = "ArtConverter";
            this.Text = "Art Converter";
            ((System.ComponentModel.ISupportInitialize)(this.pbConvertedImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btnConvertArt;
        private System.Windows.Forms.PictureBox pbConvertedImage;
    }
}

