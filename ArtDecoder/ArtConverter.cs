﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace ArtDecoder
{
    public partial class ArtConverter : Form
    {
        public ArtConverter()
        {
            InitializeComponent();

            openFileDialog1.Filter = "AOL ART Files (*.art) | *.art";
        }

        private void btnConvertArt_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
                return;
            Convert(openFileDialog1.FileName);
        }

        private void Convert(string inputFilePath)
        {
            try
            {
                var bytes = File.ReadAllBytes(inputFilePath);
                if (!JgInterop.QueryImage(bytes))
                {
                    MessageBox.Show("It doesn't look like this is an AOL .art file");
                    return;
                }
                var outputFilePath = inputFilePath + ".png";
                JgInterop.Decode(bytes, outputFilePath);
                pbConvertedImage.ImageLocation = outputFilePath;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to convert: " + ex);
            }
        }
    }
}
